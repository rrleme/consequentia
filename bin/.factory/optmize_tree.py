#!/usr/bin/env python3.7
# -*- coding: utf-8 -*-
#
#  py-tableaux.py
#
#  Copyright 2020 Renato Reis Leme <renato@logos>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#

import os
import random

from binarytree import Node
from collections import defaultdict
from itertools import chain
from random import randrange
from dataclasses import dataclass


class DeductionTree():
    """A node class for A* Pathfinding"""

    def __init__(self, parent=None, position=None):
        self.parent = parent
        self.position = position

        self.g = 0
        self.h = 0
        self.f = 0

    def __eq__(self, other):
        return self.position == other.position

#@dataclass
#class DeductionTree:
#    root: Node
#    parent: DeductionTree
#    closed: list()
#    props: defaultdict(str)
#    g: int
#    h: int
#    f: inti
#    id: int
	
U_PROPS = 0
T_props = defaultdict(str)
queue = list()
closed = list()

# output settings
output = open("output/skeleton.tex", "r")
tex_out = ""

AND = 1
NOT_1 = 2
NOT_2 = 3
IMPLY = 4
OR = 5

def g(root, node):
	return len(get_all_ancestor(root, node))

def h(prop):
	op = get_op(prop)
	if (op == OR or op == IMPLY):
		return 2
	else:
		return 1

def goal_f(tree):
	leaf = expanse_list(tree.root)
	leaf.sort()
	cl = tree.closed
	if (cl == leaf): return 1
	return 0
	
def gen_children(state):
	children = []
	
	children.append(rule_or(state, ["p", "q"]))
	
	# filho deve possuir 1 proposição :: ou não? ....
	# 1 referencia para o pai
	return children
	
def rule_or(state, props):
	new_state = DeductionTree
	
	new_state.root.left = Node(state.id+1)
	new_state.root.right = Node(state.id+2)
	
	new_state.props[state.id+1] = props[0]
	new_state.props[state.id+2] = props[1]
	
	check_branch(new_state.root, state.props, state.id)
	
	print(new_state.root)
	
	return new_state
	
	
# @start : DeductionTree
def A_Star(start):
	openSet = []
	closedSet = []
	
	openSet.append([start.f, start])
	
	while openSet:
		
		openSet.sort()
		
		currentNode = openSet[0][1]
		
		openSet.remove([currentNode.f, currentNode])
		
		closedSet.append(currentNode)
	
		
		if (goal_f(currentNode)): return currentNode # or backtrack
		
		children = gen_children(currentNode) 
		
		for child in children:
			if (closedSet.count(child)):
				continue
			else:
				
				child.g = currentNode.g + g(currentNode.g, child.root)
				child.h = h(child.props[-1])
				child.f = child.g + child.h
				if (openList.count(child)):
					continue
				else: openList.append([child.f, child])
		
		



def heuristica(root, node, prop):
	value = get_op(prop) 
	#value = value + len(prop)
	#print(g(root, node))
	#value = value + get_op(prop) 
	return value

def get_op(prop):
	op = 1

	cmd = "./app_rule \"" + prop + "\" > rule.ap"
	os.system (cmd)
			
	f = open("rule.ap", "r")
	op = f.readline()	
	
	if (op != ''):
		return int(op)	
		
	#print(prop)
	return 1

# @get_props : get propositions from deduction file;
#			   start tree
def get_props(root, fp):
	
	global T_props
	global queue
	global U_PROPS
	global tex_out
	
	lines = fp.readlines()
	
	i = 0
	
	while (i < len(lines) - 1):
		T_props[i] = "(" + lines[i].rstrip() + ")"

		if (i>0):
			root.right = Node(U_PROPS)
			root = root.right
		U_PROPS = U_PROPS + 1
		queue.append([heuristica(root, find_node(root, i), T_props[i]), i])
		i = i + 1
				
	cc = lines[i]
	T_props[i] = "~ (" + lines[i].rstrip() + ")"
	root.right = Node(U_PROPS)
	queue.append([heuristica(root, find_node(root, i), T_props[i]), i])
		
def check_branch(tree, props, node_id):
	if (len(props) == 2):
		queue.append([heuristica(tree, find_node(tree, node_id), props[0]), U_PROPS+1])
		queue.append([heuristica(tree, find_node(tree, node_id), props[1]), U_PROPS+2])
		close_branch (tree, find_node(tree, U_PROPS+1))
		close_branch (tree, find_node(tree, U_PROPS+2))
	else:
		queue.append([heuristica(tree, find_node(tree, node_id), props[0]), U_PROPS+1])
		close_branch (tree, find_node(tree, U_PROPS+1))

		
def set_or(root, node_id, props):
	global U_PROPS
	find_node(root, node_id).left = Node(U_PROPS+1)
	find_node(root, node_id).right = Node(U_PROPS+2)
	T_props[U_PROPS+1] = props[0]
	T_props[U_PROPS+2] = props[1]
	check_branch(root, props, node_id)
	U_PROPS = U_PROPS + 2
	
def set_and(root, node_id, props):
	global U_PROPS
	find_node(root, node_id).right = Node(U_PROPS+1)
	find_node(root, node_id).right.right = Node(U_PROPS+2)
	T_props[U_PROPS+1] = props[0]
	T_props[U_PROPS+2] = props[1]
	check_branch(root, props, node_id)
	U_PROPS = U_PROPS + 2
	
def set_imply(root, node_id, props):
	global U_PROPS
	find_node(root, node_id).left = Node(U_PROPS+1)
	find_node(root, node_id).right = Node(U_PROPS+2)
	T_props[U_PROPS+1] = props[0]
	T_props[U_PROPS+2] = props[1]
	check_branch(root, props, node_id)
	U_PROPS = U_PROPS + 2

def set_not(root, node_id, props):
	global U_PROPS
	find_node(root, node_id).right = Node(U_PROPS+1)
	T_props[U_PROPS+1] = props[0]
	check_branch(root, props, node_id)
	U_PROPS = U_PROPS + 1
	
def find_node(root, n_id):

	if (root == None):
		return None
	else:
		if (root.value == n_id):
			return root
	
	node = find_node(root.left, n_id)
	if (node != None):
		return node

	node = find_node(root.right, n_id)
	if (node != None):
		return node
		
	return None
	
def manage_operation(op, root, n_id, props):
	global tex_out
	if (op == 3): 
		set_and (root, n_id, props)
	if (op == 5): 
		set_or (root, n_id, props)
	if (op == 4): 
		set_imply (root, n_id, props)
	if (op == 1): 
		set_not (root, n_id, props)
	if (op == 2): 
		set_not (root, n_id, props)

# critério fraco * to-improve *
def isPropTerminal(prop):
	if (len(prop) <= 6): return (1)
	else: return (0)
	
def run_tree(root):
	global tex_out
	
	if (root == None):
		return 
		
	if (root.value == 0):
		cmd = "./to_tex \"" + T_props[0] + "\" > tmp"
		os.system (cmd)
		
		f = open("tmp", "r")
		prop = f.readline()
		
		tex_out = tex_out + "[ " + prop + "\n"
		
		run_tree(root.left)
		
		run_tree(root.right)
	
	if (not root == None):
		if (not root.value == 0):
			cmd = "./to_tex \"" + T_props[root.value] + "\" > tmp"
			os.system (cmd)
		
			f = open("tmp", "r")
			prop = f.readline()	
			
			if (prop == 'x'):
				tex_out = tex_out + "[ " + "\\textbf{x}" + "\n"
			else:
				tex_out = tex_out + "[ " + prop + "\n"
			
			run_tree(root.left)
		
			run_tree(root.right)
			
			tex_out = tex_out + "]" 


def is_closed(root, node):
	branch = get_all_ancestor(root, node)
	branch.append(node)
	for i in branch:
		if (closed.count(i.value)):
			return 1
	return 0

# get all leaf of sub-tree
def expanse_list(node):
	leaf = []
	if (node == None):
		return []
	if (node.left == None and node.right == None):
		return list(node)
	leaf.append(expanse_list(node.right))
	leaf.append(expanse_list(node.left))
	return list(chain(*leaf))

def find_parent(root, node):
	
	if (root == None):
		return None
	else:
		if (not root.left == None):
			if (root.left.value == node.value):
				return root
		if (not root.right == None):
			if (root.right.value == node.value):
				return root
		
	n = find_parent(root.left, node)
	if (n != None):
		return n

	n = find_parent(root.right, node)
	if (n != None):
		return n
		
	return None
	
def get_all_ancestor(root, node):
	ancestors = []
	
	while (node != root):
		node = find_parent(root, node)
		ancestors.append(node)

	return ancestors

def is_neg(prop):
	is_neg_true = 0
	i = 0
	while (i < len(prop) and is_neg_true == 0):
		if (prop[i] == "~"):
			is_neg_true = 1
		i = i + 1
	return is_neg_true
	
def same_var(prop1, prop2):
	if (str(''.join(filter(str.isalpha, prop1))) == str(''.join(filter(str.isalpha, prop2)))):
		return 1
		
	return 0

def get_sub_tree(node):
	subtree = []
	if (node == None):
		return subtree
	subtree.append(get_sub_tree(node.left))
	subtree.append(get_sub_tree(node.right))
	return subtree
		
# given two propositions, 
# decide (syntactically) if they are contradicting one another
def insert_bottom(prop1, prop2):
	if (is_neg(prop1) and (not is_neg(prop2))):
		return 1
	if (is_neg(prop2) and (not is_neg(prop1))):
		return 1
	return 0

## Look only to those propositions that are terminals
def close_branch(root, node):
	for i in get_all_ancestor(root, node):
		if (isPropTerminal (T_props[node.value]) and isPropTerminal (T_props[i.value]) and same_var(T_props[i.value], T_props[node.value])):
			if (insert_bottom (T_props[i.value], T_props[node.value])):
				closed.append(node.value)
		

def make_tree(args):
	
	global T_props
	global queue
	global tex_out
	global U_PROPS
	global closed

	closed = list()
	
	queue = list()
	
	T_props = defaultdict(str)

	U_PROPS = 0

	# output settings
	output = open("output/skeleton.tex", "r")
	tex_out = ""
	
	props = []
	
	root = defaultdict(list)
	tree = Node(0)

	propos = defaultdict(str)
	
	## Get propositions from *.d file
	fp = open("deduction.d", "r")
	get_props (tree, fp)
	fp.close()
	
	while (queue):
	
		new_props = []
		
		#random.shuffle(queue)
		#i = queue[randrange(0,len(queue))]
		queue.sort()
		i = queue[0][1]
		
		#print(queue)
		
		if (not isPropTerminal(T_props[i])):
			
			#print(find_node(tree, i))
			
			leaf_nodes = expanse_list(find_node(tree, i))
			
			#print(leaf_nodes)
			
			cmd = "./app_rule \"" + T_props[i] + "\" > rule.ap"
			os.system (cmd)
			
			f = open("rule.ap", "r")
			op = int(f.readline())		
		
			lines = f.readlines()

			for line in lines:
				new_props.append(str(line).rstrip())
		
			for leaf in leaf_nodes:
				if (not is_closed(tree, leaf)):
					manage_operation(op, tree, leaf.value, new_props)	
				#print(str(i) + "-esimo LEAF " + str(leaf.value))
				
			f.close()
			
			
			queue.remove([heuristica(tree, find_node(tree, i), T_props[i]), i])
		
		else: 
			
			#print([heuristica(tree, find_node(tree, i), T_props[i]), i])
			
			
			queue.remove([heuristica(tree, find_node(tree, i), T_props[i]), i])
		
	#print(tree)
	
	for i in closed:
		#print (i)
		node = find_node(tree, i)
		if (node != None):
			node.right = Node(U_PROPS+1)
			T_props[U_PROPS+1] = 'x'
			U_PROPS = U_PROPS + 1
	
	#close_branch(tree, find_node(tree, 5))
	
	#close_branch (tree, find_node(tree, 10), T_props[11])
	
	#print (find_parent(tree, find_node(tree, 10)).value)
	
	run_tree (tree)
	tex_out = tex_out + "]"
	


def main(args):
	
	start = DeductionTree
	start.root = Node(0)
	start.f = 0
	start.g = 0
	start.h = 0
	start.id = 0
	start.props = {0:"p"}
	start.closed = []
	
	A_Star(start)
	
	
	#return
	
	i = 0
	while (i < 1):
		
		make_tree(args)
		
	
		f_name = "run_tests/" + str(i)
		f = open(f_name, "w")
		
		f.write(tex_out)

		count = 0

		for k in tex_out.splitlines():
			count = count + 1
			
		count = count - 1
		
		#if (count <= 41):
		#	print ("ENCONTRADO: ")
		
		print (str(i) + "-size: " + str(count))

		i = i + 1
		
		print(tex_out)	

if __name__ == '__main__':
    import sys
    sys.exit(main(sys.argv))
