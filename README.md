# Consequentia

<img src="res/logo.png" align="right" />

[![License: GPL v3](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)
[![PyPI pyversions](https://img.shields.io/github/pipenv/locked/python-version/metabolize/rq-dashboard-on-heroku)](https://pypi.python.org/pypi/ansicolortags/)

> An automated system for Tableaux.

## Table of contents

[[_TOC_]]

## Some of its central features

- **LaTeX** output ([forest](https://ctan.org/pkg/forest) library)
- Kernel upon GNU/Bison.
- Open-source.

## List of supported logics (so far)

1. Classical Propositional Logic

## Definitions

### Analytical Tableaux

A tableau is a method to decide if a set of propositions entails another. Let $`\Gamma`$ be a set of proposition (possible empty)  and $`\varphi`$ a proposition. Then

```math
\Gamma \vdash \varphi
```

if, and only if, the tableau is a *closed* tree.

## Correctness

to-do.

# Examples

<div align="center">
![Examples](res/ex_trees.png)
</div>

# Usage

First of all, set your deduction in [deduction.d](deduction.d) file. Then run

```console
user@host:~$ bash consequentia.sh
```

The output will be the **LaTeX** code for *forest* library.
